#include <stdio.h>

#define TABSIZE 4
#define MAXLINE 1000

int detab(void);
char line[MAXLINE];
char detabbedLine[MAXLINE];

int getLine(void);

int main()
{
	extern char detabbedLine[MAXLINE];
	int t, i;
	int d = 0;
	int lineLen = getLine();
	for(i = 0; i < lineLen; i++, d++){
		if(line[i] == '\t'){
			for(t = 0; t < TABSIZE; t++){
				detabbedLine[d+t] = ' ';
		 	}	
			d+=TABSIZE;
			i++;
		}
		detabbedLine[d] = line[i];
	}
	printf("%s", detabbedLine);
	return 0;
	
}

int getLine(void)
{
	int c, i;
	extern char line[];
	for (i = 0; i < MAXLINE-1
			&& (c=getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;
	if (c == '\n') {
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}
