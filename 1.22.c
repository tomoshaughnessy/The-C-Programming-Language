#include <stdio.h>

#define TABSIZE 8
#define MAXLINE 1000
#define LINESIZE 4

int fold(void);
char line[MAXLINE];
char folded_line[MAXLINE];

int getLine(void);

int main()
{
	extern char folded_line[MAXLINE];
	int t = 0;
	int i = 0;
	int s = 0;
	int j = 0;
	int lineLen = getLine();
	for (i = 0; i < lineLen; i++,j++){
		if (i % LINESIZE == LINESIZE - 1){
			if (line[i] != ' ' || line[i] != '\t'){
				folded_line[j] = '-';
				j++;
				folded_line[j] = '\n';
				j++;
			}
			if (line[i] == ' ' || line[i] == '\t'){
				folded_line[j] = '\n';
				continue;
			}
			folded_line[j] = line[i];
		}
		folded_line[j] = line[i];
	}
	printf("%s", folded_line);
	return 0;
}

int getLine(void)
{
	int c, i;
	extern char line[];
	for (i = 0; i < MAXLINE-1
			&& (c=getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;
	if (c == '\n') {
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}
