#include <stdio.h>

#define TABSIZE 8
#define MAXLINE 1000

int detab(void);
char line[MAXLINE];
char entabbed_line[MAXLINE];

int getLine(void);

int main()
{
	extern char detabbedLine[MAXLINE];
	int t = 0;
	int i = 0;
	int s = 0;
	int j = 0;
	int lineLen = getLine();
	for (i = 0; i < lineLen; j++,i++){
		if(line[i] == ' '){
			s++;
			j--;
			continue;
		}

		for(t = 0; t < s/TABSIZE; t++)
			entabbed_line[j+t]='\t';
		j+=s/TABSIZE;
		

		for (t = 0; t < s%TABSIZE; t++)
			entabbed_line[j+t]= ' ';
		j+=s%TABSIZE;

		if (line[i] == '\t' && s < TABSIZE)
			entabbed_line[j] = '\t';
		if (line[i] != ' ' || line[i] != '\t')
			entabbed_line[j] = line[i];
		
		s = 0;
	}
	printf("%s", entabbed_line);
	return 0;
}

int getLine(void)
{
	int c, i;
	extern char line[];
	for (i = 0; i < MAXLINE-1
			&& (c=getchar()) != EOF && c != '\n'; ++i)
		line[i] = c;
	if (c == '\n') {
		line[i] = c;
		++i;
	}
	line[i] = '\0';
	return i;
}
